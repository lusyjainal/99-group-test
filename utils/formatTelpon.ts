const formatTelpon = (telpon: any) => {
  return telpon.replace(/\b\d{4}\s\d{4}\b/g, (match: any) => {
    // Gantilah 4 digit terakhir dengan 'XXXX'
    return match.replace(/\d{4}$/, "XXXX");
  });
};

export default formatTelpon;
