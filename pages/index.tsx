import MetaData from '@/components/templates/Layout';
import PropertyCardTemplate from "@/components/templates/PropertyCardTemplate";
import type { GetServerSideProps, InferGetServerSidePropsType } from "next";

type PropertiInfo = {
  pic: string[];
  title: string;
  address: string;
  project_type: string;
  year: number;
  ownership_type: string;
  psf_min: number;
  psf_max: number;
  subprice_label: string;
  availabilities_label: string;
  description: string;
};

export const getServerSideProps = (async () => {
  // Fetch data from external API
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_API}/core-asset/static/test/mock-api.json`,
  );
  const propertiInfo: PropertiInfo = await res.json();
  // Pass data to the page via props
  return { props: { propertiInfo } };
}) satisfies GetServerSideProps<{ propertiInfo: PropertiInfo }>;


export default function Home({
  propertiInfo,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {

  return (
    <>
      <MetaData
        data={propertiInfo}
      />
      <div className="p-4">
        <PropertyCardTemplate data={propertiInfo} />
      </div>
    </>
  );
}

