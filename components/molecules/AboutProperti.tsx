"use client";
import Flat from "@/public/assets/images/Flat.svg";

const AboutProperti = ({
  title,
  address,
  project_type,
  year,
  ownership_type,
  subprice_label,
  availabilities_label,
  psf_min,
  psf_max,
}: any) => {
  return (
    <div className="p-4 flex-col gap-4 w-full">
      <div className="flex md:flex-row flex-col gap-3 justify-between">
        <div className="flex-col justify-start items-start gap-3 flex">
          <div className="justify-start items-center gap-4 inline-flex">
            <div className="w-10 h-10 relative">
              <div className="w-10 h-10 left-0 top-0 absolute bg-blue-300 rounded-xl" />
              <div className="w-[22px] h-[22px] left-[9px] top-[9px] absolute">
                <div className="w-[22px] h-[22px] left-0 top-0 absolute">
                  <img src={Flat.src} alt="flat" />
                </div>
              </div>
            </div>
            <div className="flex-col justify-start items-start gap-1 inline-flex">
              <div className="flex-col justify-start items-start gap-1 flex">
                <div className="text-blue-950 md:text-[23px] tex-[16px] font-semibold font-['Avenir Next']">
                  {title}
                </div>
              </div>
              <div className="text-right text-slate-500 md:text-[14px] text-[12px] font-normal font-['Avenir Next']">
                {address}
              </div>
            </div>
          </div>
          <div className="flex-col justify-start items-start gap-0.5 flex">
            <div className="text-blue-950 md:text-[16px] text-[14px] text-base font-normal font-['Avenir Next']">
              {project_type} · {year} · {ownership_type}
            </div>
            <div className="text-blue-950 md:text-[16px] text-[14px] text-base font-normal font-['Avenir Next']">
              {availabilities_label}
            </div>
          </div>
        </div>
        {/*  */}
        <div className="flex md:flex-col flex-row justify-start gap-2 items-center inline-flex">
          <div className="md:text-right text-blue-950 md:text-[19px] text-[16px] font-semibold font-['Avenir Next']">
            {psf_min.toLocaleString("en-US", {
              style: "currency",
              currency: "USD",
              minimumFractionDigits: 0,
            })}{" "}
            -{" "}
            {psf_max.toLocaleString("en-US", {
              style: "currency",
              currency: "USD",
              minimumFractionDigits: 0,
            })}{" "}
            psf
          </div>
          <div className="md:text-right text-slate-500 md:text-[14px] text-[12px] font-normal font-['Avenir Next']">
            {subprice_label}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutProperti;
