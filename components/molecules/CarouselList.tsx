"use client";
import React from "react";

import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation } from "swiper/modules";
import Image from 'next/image';

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

const CarouselList = ({ pic }: any) => {
  return (
    <Swiper
      pagination={{
        clickable: true,
      }}
      navigation={true}
      modules={[Pagination, Navigation]}
      className="mySwiper"
    >
      {pic.map((item: any, index: any) => (
        <SwiperSlide key={index}>
          <Image
            src={item}
            alt="Deskripsi gambar"
            width={400} // Ganti dengan lebar yang sesuai
            height={272} // Ganti dengan tinggi yang sesuai
            loading="lazy"
            className="w-full md:h-[272px] h-[160px] left-[4px] rounded-tl rounded-tr"
            blurDataURL="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gHYSUNDX1BST0ZJTEUAAQEAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAH4AABAAEAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADb/2wBDABQODxIPDRQSEBIXFRQYHjIhHhwcHj0sLiQySUBMS0dARkVQWnNiUFVtVkVGZIhlbXd7gYKBTmCNl4x9lnN+gXz/2wBDARUXFx4aHjshITt8U0ZTfHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHz/wAARCABBAIIDASIAAhEBAxEB/8QAGgAAAgMBAQAAAAAAAAAAAAAAAAMCBAUBBv/EACEQAQACAgICAwEBAAAAAAAAAAABAgNhBBETQRIxURQh/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAaEQEBAQEBAQEAAAAAAAAAAAAAEQESAiFR/9oADAMBAAIRAxEAPwDe6RmDfijMOteaE2gqx9oKtC0hFirH2gmy5pCpRSlFqkdhOqEGVKkMqbVCsG1hN1YZUysI1g2sM1Y7EOuxCUQzViIS6BSO9ITCfaEyxWoXaCbnWki8rVhVyLG3lXvZavKNpQ7ctZH5NU5Ng2qvWxtLFOVmh1Fekn0lKkPqbWCaybWWaQ2ISiEIlKJSkd6A7BViv5EJyKnn2jObbKrNshF7k2zbJvmFMvci9yr5le+batZh1siPkVL5tl+eP1pqNGuQ2mRl1zx+n0z7E3GrTIfS7Lpm2fTNtGY063Ordm1zbNrm2lRoRdL5qMZtpRm2gu/MKfmBUY39W0Z5W2B/bP65PNn9Rm7+N23J2Tfk7Ys8yxduTkn301mN5Wvk5Ufqrk5f5/rOnJafuXO5a+Y3mxZvyJn7lCc2yAdL3p8Z+vcm05c199qYOjvWrj50e56W8fKifbz6dcl6fVpZ1ndelpydnV5G3nMXLt9StU5Us7rnuxvRyNpRyNsevJ79mRn2zU6av9Gwy/PsFOnnQA6OgAAAAAAAAAAAAA7X7haoAx7c/Z9DYAc8ccSACq//2Q=="
            placeholder="blur"
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default CarouselList;
