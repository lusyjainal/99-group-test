"use client";
import CarouselList from "@/components/molecules/CarouselList";
import AboutProperti from "@/components/molecules/AboutProperti";
import Badge from "@/components/atoms/Badge";
import SeeDescription from "@/components/atoms/SeeDescription";
import data from "@/utils/mockData";

const PropertiList = () => {
  return (
    <div className="w-full md:h-[272px] h-[160px]  top-0 absolute">
      <CarouselList pic={data.pic} />
      <Badge />
      <AboutProperti
        title={data?.title}
        address={data?.address}
        project_type={data?.project_type}
        year={data?.year}
        ownership_type={data?.ownership_type}
        subprice_label={data?.subprice_label}
        availabilities_label={data?.availabilities_label}
        psf_min={data?.psf_min}
        psf_max={data?.psf_max}
      />
      <SeeDescription description={data?.description} />
    </div>
  );
};

export default PropertiList;
