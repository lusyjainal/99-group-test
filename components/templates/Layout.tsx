import formatTelpon from '@/utils/formatTelpon'
import Head from 'next/head'
import React from 'react'

export default function MetaData({ data }: any) {
  return (
    <Head>
      <title>{data?.title}</title>
      <meta name="description" content={formatTelpon(data.description)}/>
      <meta name="title" content={data?.title}/>
      <meta name="image" content={data.pic[0]} />

      {/* <!-- Facebook Meta Tags --> */}
      <meta property="og:title" content={data?.title} />
      <meta property="og:description" content={formatTelpon(data.description)} />
      <meta property="og:image" content={data.pic[0]} />
      <meta property="og:url" content="jajal" />
      <meta property="og:site_name" content="jajal" />
      <meta property="og:type" content="website" />

      {/* <!-- Twitter Meta Tags --> */}
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content={data?.title}/>
      <meta name="twitter:description" content={formatTelpon(data.description)} />
      <meta name="twitter:image" content={data.pic[0]} />
      {/* <!-- Meta Tags Generated via https://toolsaday.com -->  */}
    </Head>
  )
}
