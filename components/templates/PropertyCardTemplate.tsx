// "use client";
import React from "react";

import PropertiList from "@/components/organisms/PropertiList";

const PropertyCardTemplate = ({ data }: any) => {
  return (
    <div className="md:w-[544px] md:h-[470px] w-full h-[380px] relative bg-white rounded shadow ">
      {/* start of thumbnail */}
      <PropertiList />
      {/* end of thumbnail */}
    </div>
  );
};

export default PropertyCardTemplate;
