"use client";

import Bottom from "@/public/assets/images/Bottom.svg";
import Flagtip from "@/public/assets/images/Flagtip.svg";

const Badge = () => {
  return (
    <div className="w-[134px] h-[25px] top-[6px] left-[-4px] absolute z-40">
      <div className="left-0 top-0 absolute flex-col justify-start items-start inline-flex">
        <div className="justify-start items-center inline-flex">
          <div className="px-0.5 py-[2.50px] bg-pink-400 rounded-tl-sm justify-start items-center flex">
            <div className="w-[3px] h-[7px]" />
            <div className="text-white text-xs font-semibold font-['Avenir Next'] uppercase tracking-wide">
              launching soon
            </div>
          </div>
          <img src={Flagtip.src} alt="flag-tip" />
        </div>
        <div className="w-1 h-1 relative">
          <img src={Bottom.src} alt="bottom" />
        </div>
      </div>
    </div>
  );
};

export default Badge;
