"use client";
import formatTelpon from "@/utils/formatTelpon";
import { useState } from "react";

const SeeDescription = ({ description }: any) => {
  const [isExpanded, setIsExpanded] = useState(false);

  return (
    <>
      <div className="pr-4 w-full justify-end items-center inline-flex">
        <div
          onClick={() => setIsExpanded(!isExpanded)}
          className="md:text-[16px] text-[14px] text-center text-blue-600 text-base font-semibold font-['Avenir Next'] cursor-pointer"
        >
          {isExpanded ? "Less" : "See"} description
        </div>
      </div>
      <div>
        {isExpanded ? (
          <p className="bg-white rounded shadow p-4 text-blue-950 md:text-[16px] text-[14px] text-base font-normal font-['Avenir Next']">
            {formatTelpon(description)}
          </p>
        ) : null}
      </div>
    </>
  );
};

export default SeeDescription;
